package modularization.features.dashboard.data.interfaces;

import modularization.features.dashboard.data.model.Dog;

import java.io.IOException;

public interface StorageBase {
    Dog getRandomDog() throws IOException;
}
