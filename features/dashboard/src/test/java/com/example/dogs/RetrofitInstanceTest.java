package com.example.dogs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import modularization.features.dashboard.data.interfaces.GetDogDataService;
import modularization.features.dashboard.data.model.Dog;
import modularization.features.dashboard.data.network.RetrofitInstance;
import modularization.features.dashboard.data.repo.RetrofitDogStorage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class RetrofitInstanceTest {
    private MockWebServer server;
    OkHttpClient client;
    MockResponse mockedResponse;
    private String jsonFile;
    String json;
    HttpUrl baseUrl;

    @Before
    public void setup() throws Exception {
        server = new MockWebServer();
        server.start();
        client = new OkHttpClient();
        jsonFile = "response.json";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(jsonFile);
        json = Utils.readTextStream(inputStream);
        assertTrue(Utils.isJSONValid(json));
        mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200)
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
                .setBody(json);
        server.enqueue(mockedResponse);
        baseUrl = server.url("/v1/images/search/");
    }

    @After
    public void shutdown() throws IOException {
        server.shutdown();
    }

    @Test
    public void testJson() throws Exception {
        Request request = new Request.Builder()
                .url(baseUrl)
                .build();
        Call call = client.newCall(request);
        Response response = call.execute();
        assertEquals(json, response.body().string());
    }

    @Test
    public void testretrofit() throws IOException {
        GetDogDataService service = RetrofitInstance
                .getRetrofitInstance(baseUrl.toString())
                .create(GetDogDataService.class);

        retrofit2.Call<List<Dog>> call = service.getDogData();
        retrofit2.Response<List<Dog>> response = call.execute();
        assertNotNull(response);
        assertEquals(1, response.body().size());
    }

    @Test
    public void testRetrofitStorage() throws IOException {
        RetrofitDogStorage storage = new RetrofitDogStorage(null, null);
        Dog response = storage.getRandomDog();
        assertNotNull(response);
        System.out.println(response.toString());
    }
}