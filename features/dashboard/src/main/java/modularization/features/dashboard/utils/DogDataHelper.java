package modularization.features.dashboard.utils;

import modularization.features.dashboard.data.model.Breed;
import modularization.features.dashboard.data.model.Dog;

public class DogDataHelper {
    public static String getDodDescription(Dog dog) {
        String strDog = String.format("Width:%s, Height:%s \n", dog.getWidth(), dog.getHeight());
        String strBreed = "";
        if (dog.getBreedList() != null) {
            for (Breed b : dog.getBreedList()) {
                strBreed += String.format("Name:%s, Temperament:%s \n", b.name, b.temperament);
                strBreed += String.format("Weight:%s, Height:%s \n", b.weight.metric, b.height.metric);
                strBreed += String.format("Group:%s, Life span:%s", b.breed_group, b.life_span);
            }
        }
        return strDog + strBreed;
    }
}
