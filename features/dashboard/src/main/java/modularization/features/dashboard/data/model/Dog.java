package modularization.features.dashboard.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import modularization.features.dashboard.data.model.Breed;

public class Dog {

    @SerializedName("id")
    private String id;
    @SerializedName("url")
    private String url;
    @SerializedName("width")
    private String width;
    @SerializedName("height")
    private String height;
    @SerializedName("breeds")
    private ArrayList<Breed> breedList;

    public Dog(String id, String weight, String hight, String url) {
        this.id = id;
        this.url = url;
        this.width = weight;
        this.height = hight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public ArrayList<Breed> getBreedList() {
        return breedList;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", weigth='" + width + '\'' +
                ", height='" + height + '\'' +
                '}';
    }
}
