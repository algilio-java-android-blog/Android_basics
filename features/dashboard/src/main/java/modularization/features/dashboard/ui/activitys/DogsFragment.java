package modularization.features.dashboard.ui.activitys;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import modularization.features.dashboard.R;
import modularization.features.dashboard.data.model.Dog;
import modularization.features.dashboard.ui.interfaces.OnDogChange;
import modularization.features.dashboard.utils.DogDataHelper;
import com.squareup.picasso.Picasso;

public class DogsFragment extends Fragment implements OnDogChange {
    private static final String CLASS_NAME = DogsFragment.class.getName();

    public static final String ARG_DESC = "DOG_DESC";
    public static final String ARG_PICTURE = "DOG_PICTURE";

    private TextView mTvDogDesc;
    private ImageView mIvDogPict;

    private String mDesc;
    private String mPic;

    public static Fragment newInstance(String desc, String pic) {
        DogsFragment fragment = new DogsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DESC, desc);
        args.putString(ARG_PICTURE, pic);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mDesc = args.getString(ARG_DESC);
            mPic = args.getString(ARG_PICTURE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater
            , @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fr_dogs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTvDogDesc = view.findViewById(R.id.tvDogName);
        mIvDogPict = view.findViewById(R.id.ivDogPic);
        mTvDogDesc.setText(mDesc);
        try {
            int drawableResource = getActivity().getResources()
                    .getIdentifier(mPic, "drawable", getActivity().getPackageName());
            if (drawableResource != 0) {
                mIvDogPict.setImageResource(drawableResource);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateFragment(Dog dog) {
        mTvDogDesc.setText(DogDataHelper.getDodDescription(dog));
        Picasso.with(getActivity())
                .load(dog.getUrl())
                .into(mIvDogPict);
    }

    @Override
    public void updateStatusBar(Dog dog) {
    }
}
