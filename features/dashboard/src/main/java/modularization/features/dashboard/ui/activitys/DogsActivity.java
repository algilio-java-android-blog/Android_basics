package modularization.features.dashboard.ui.activitys;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import modularization.features.dashboard.data.factory.StorageFactory;
import modularization.features.dashboard.data.model.Dog;
import modularization.features.dashboard.data.interfaces.StorageBase;

import modularization.features.dashboard.ui.interfaces.OnDogChange;
import modularization.features.dashboard.utils.DogDataHelper;

import java.io.IOException;

public class DogsActivity extends SingleFragmentActivity implements OnDogChange {

    private StorageBase mDogs;
    private Dog mDog;
    private Fragment mFragment;

    @Override
    protected Fragment getFragment() throws IOException {
        mDog = mDogs.getRandomDog();
        mFragment = DogsFragment
                .newInstance(DogDataHelper.getDodDescription(mDog)
                        , mDog.getUrl());
        return mFragment;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mDogs = new StorageFactory().getStorage(this, this, "retrofit");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void updateFragment(Dog dog) {
        mDog = dog;
        if (mFragment != null && mFragment instanceof OnDogChange) {
            ((OnDogChange) mFragment).updateFragment(mDog);
        }
    }

    @Override
    public void updateStatusBar(Dog dog) {
        mDog = dog;
        this.updateStatusBar(String.format("ID:%s", mDog.getId()));
    }
}
