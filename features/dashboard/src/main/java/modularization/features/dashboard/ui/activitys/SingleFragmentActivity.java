package modularization.features.dashboard.ui.activitys;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import modularization.features.dashboard.R;
import modularization.libraries.services.TimerBroadcastService;
import modularization.libraries.ui_components.dialogs.SeekBarDialog;
import modularization.libraries.utils.SharedPreferencesHelper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public abstract class SingleFragmentActivity extends AppCompatActivity {
    public static final String TAG = SingleFragmentActivity.class.getSimpleName();
    public static final String TIMER_RUNNING = "mTimerRunning";
    //public static final String TIMER_COUNT_DOWN_INTERVAL = "time_count_down_interval";
    //public static final String TIMER_SECONDS_UNTIL_FINISHED = "time_seconds_until_finished";


    private SharedPreferencesHelper mSharedPreferencesHelper;

    TextView tvBarTimer;
    TextView tvBarDog;
    private MenuItem btChange;

    private long mCountDownInterval;
    private long mSecsUntilFinished;
    private long mCountDownMax;
    private boolean mTimerRunning;
    private Intent mTimerIntent;
    private int mTick;

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                mSecsUntilFinished = TimeUnit.MILLISECONDS.toSeconds(intent.getLongExtra(
                        TimerBroadcastService.TIMER_SECONDS_UNTIL_FINISHED,
                        0));
                int tick = intent.getIntExtra(
                        TimerBroadcastService.TIMER_TICK_NO,
                        0);
                mTick++;
                if (mTick != tick) {
                    Log.e("Error",
                            String.format(" Timer tick count mismatch:%d!=%d ", mTick, tick));
                }
                updateGUI();
            }
        }
    };

    SeekBarDialog.OnSeekBarSetListener mOnCountDownIntervalSetListener = new SeekBarDialog.OnSeekBarSetListener() {
        @Override
        public void onProgressSet(SeekBar view, int progress) {
            if (mSharedPreferencesHelper != null) {
                mCountDownInterval = progress;
                mSharedPreferencesHelper.saveCountDownInterval(mCountDownInterval);
                updateTimerStatusBar(mCountDownMax);
            }
        }
    };

    SeekBarDialog.OnSeekBarSetListener mOnMaxIntervalSetListener = new SeekBarDialog.OnSeekBarSetListener() {
        @Override
        public void onProgressSet(SeekBar view, int progress) {
            if (mSharedPreferencesHelper != null) {
                mCountDownMax = progress;
                mSharedPreferencesHelper.saveCountDownMaxInterval(mCountDownMax);
                updateTimerStatusBar(mCountDownMax);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferencesHelper = new SharedPreferencesHelper(this);
        mCountDownMax = mSharedPreferencesHelper.getCountDownMax();
        mCountDownInterval = mSharedPreferencesHelper.getCountDownInterval();
        mTimerIntent = new Intent(this, TimerBroadcastService.class);

        setContentView(R.layout.ac_single_fragment);

        tvBarTimer = findViewById(R.id.tvStatusBarTimer);
        tvBarDog = findViewById(R.id.tvStatusBarDog);

        if (savedInstanceState == null) {
            try {
                switchFragment();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTimerRunning = savedInstanceState.getBoolean(TIMER_RUNNING);
        mSecsUntilFinished = savedInstanceState.getLong(TimerBroadcastService.TIMER_SECONDS_UNTIL_FINISHED);
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mBroadcastReceiver, new IntentFilter(TimerBroadcastService.COUNTDOWN_BR));
        updateTimerStatusBar(mCountDownMax);
        if (mTimerRunning) {
            startTimer();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        btChange = menu.findItem(R.id.action_start);
        if (mTimerRunning) {
            btChange.setTitle(R.string.stop);
        } else {
            btChange.setTitle(R.string.start);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_start) {
            if (!mTimerRunning) {
                mSecsUntilFinished = mCountDownMax;
                startTimer();
            } else {
                mTimerRunning = false;
                stopTimer();
            }
        } else if (itemId == R.id.count_interval) {
            mTimerRunning = false;
            mSecsUntilFinished = 0;
            stopTimer();
            countDownIntervalDialog();
        } else if (itemId == R.id.max_interval) {
            mTimerRunning = false;
            mSecsUntilFinished = 0;
            stopTimer();
            countDownMaxDialog();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
        stopTimer();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(TIMER_RUNNING, mTimerRunning);
        outState.putLong(TimerBroadcastService.TIMER_SECONDS_UNTIL_FINISHED, mSecsUntilFinished);
    }


    private void startTimer() {
        mTimerIntent.putExtra(TimerBroadcastService.TIMER_SECONDS_UNTIL_FINISHED, mSecsUntilFinished);
        mTimerIntent.putExtra(TimerBroadcastService.TIMER_COUNT_DOWN_INTERVAL, mCountDownInterval);
        startService(mTimerIntent);
        mTick = 0;
        mTimerRunning = true;
        invalidateOptionsMenu();
    }

    private void stopTimer() {
        stopService(mTimerIntent);
        invalidateOptionsMenu();
    }

    void switchFragment() throws IOException {
        Fragment fragment = getFragment();
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (!fragmentManager.isDestroyed()) {
                fragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainer, fragment)
                        .commit();
            }
        } else {
            Toast.makeText(this, "Did not change", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateTimerStatusBar(long untilFinished) {
        @SuppressLint("DefaultLocale") String status = String
                .format("%d/%d   interval: %d"
                        , mCountDownMax
                        , untilFinished
                        , mCountDownInterval);
        tvBarTimer.setText(status);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void countDownIntervalDialog() {
        SeekBarDialog dlg = new SeekBarDialog
                .SeekBarDialogBuilder(this)
                .title(getString(R.string.count_down_time))
                .progress(mCountDownInterval)
                .min(1)
                .max((int) mCountDownMax / 10)
                .callback(mOnCountDownIntervalSetListener)
                .build();
        dlg.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void countDownMaxDialog() {
        SeekBarDialog dlg = new SeekBarDialog
                .SeekBarDialogBuilder(this)
                .title(getString(R.string.max_count_down_time))
                .progress(mCountDownMax)
                .min((int) mCountDownInterval * 10)
                .max(500)
                .callback(mOnMaxIntervalSetListener)
                .build();
        dlg.show();
    }

    private void updateGUI() {
        updateTimerStatusBar(mSecsUntilFinished);
        if (mSecsUntilFinished > 0) {
            try {
                switchFragment();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mTimerRunning = false;
            stopTimer();
            updateTimerStatusBar(0);
        }
    }

    protected void updateStatusBar(String dog) {
        tvBarDog.setText(dog);
    }

    protected abstract Fragment getFragment() throws IOException;
}
