package modularization.features.dashboard.data.interfaces;

import modularization.features.dashboard.data.model.Dog;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDogDataService {

    @GET("/v1/images/search/")
    Call<List<Dog>> getDogData();
}
