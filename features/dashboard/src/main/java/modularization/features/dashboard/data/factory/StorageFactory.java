package modularization.features.dashboard.data.factory;

import android.content.Context;

import modularization.features.dashboard.data.repo.LocalDogsStorage;
import modularization.features.dashboard.data.repo.RetrofitDogStorage;
import modularization.features.dashboard.data.interfaces.StorageBase;
import modularization.features.dashboard.ui.interfaces.OnDogChange;

public class StorageFactory {

    public StorageBase getStorage(Context context, OnDogChange callback, String storageType) {
        if (storageType == null) {
            return null;
        }

        if (storageType.equalsIgnoreCase("local")) {
            return new LocalDogsStorage(callback);
        } else if (storageType.equalsIgnoreCase("retrofit")) {
            return new RetrofitDogStorage(context, callback);
        }
        return null;
    }
}

