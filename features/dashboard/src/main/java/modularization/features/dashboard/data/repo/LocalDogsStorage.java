package modularization.features.dashboard.data.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import modularization.features.dashboard.data.interfaces.StorageBase;
import modularization.features.dashboard.data.model.Dog;
import modularization.features.dashboard.ui.interfaces.OnDogChange;

import java.util.ArrayList;
import java.util.Random;

public class LocalDogsStorage implements StorageBase {

    private ArrayList<Dog> mDogs;
    private int mMaxRandom;
    private OnDogChange callback;

    public LocalDogsStorage(OnDogChange callback) {
        mDogs = new ArrayList<>();
        mDogs.add(new Dog("dog1", "1280", "906", "pngegg"));
        mDogs.add(new Dog("dog2", "500", "300", "cute_corgi_dog"));
        mDogs.add(new Dog("dog3", "700", "500", "daco_2870205"));

        mMaxRandom = mDogs.size();
        this.callback = callback;
    }

    @Override
    public Dog getRandomDog() {
        Random rand = new Random();
        Dog dog = mDogs.get(rand.nextInt(mMaxRandom));
        callback.updateStatusBar(dog);
        return dog;
    }
}
