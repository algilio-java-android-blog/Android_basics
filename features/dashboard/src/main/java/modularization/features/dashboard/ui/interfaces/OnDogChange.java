package modularization.features.dashboard.ui.interfaces;

import modularization.features.dashboard.data.model.Dog;

public interface OnDogChange {
   void updateFragment(Dog dog);
   void updateStatusBar(Dog dog);
}
