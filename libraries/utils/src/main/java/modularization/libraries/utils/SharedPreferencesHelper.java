package modularization.libraries.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {
    private static final long SECONDS = 500L;
    public static final String SHARED_PREF_NAME = "SHARED_PREF_DOGS";
    public static final String INTERVAL_PROGRESS_KEY = "INTERVAL_PROGRESS_KEY_LONG";
    public static final String INTERVAL_MAX_KEY = "INTERVAL_MAX_KEY";


    private SharedPreferences mSharedPreferences;

    public SharedPreferencesHelper(Context context) {
        mSharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public long getCountDownInterval() {
        return mSharedPreferences.getLong(INTERVAL_PROGRESS_KEY, 5L);
    }

    public boolean saveCountDownInterval(long interval) {
        mSharedPreferences.edit().putLong(INTERVAL_PROGRESS_KEY, interval).apply();
        return true;
    }

    public long getCountDownMax() {
        return mSharedPreferences.getLong(INTERVAL_MAX_KEY, SECONDS);
    }

    public boolean saveCountDownMaxInterval(long max) {
        mSharedPreferences.edit().putLong(INTERVAL_MAX_KEY, max).apply();
        return true;
    }
}
