package modularization.features.dashboard.data.repo;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import modularization.features.dashboard.data.interfaces.GetDogDataService;
import modularization.features.dashboard.data.interfaces.StorageBase;
import modularization.features.dashboard.data.model.Dog;
import modularization.features.dashboard.data.network.RetrofitInstance;
import modularization.features.dashboard.ui.interfaces.OnDogChange;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitDogStorage implements StorageBase {

    private OnDogChange callback;
    private GetDogDataService service;
    private Context context;
    Call<List<Dog>> mCall;


    public RetrofitDogStorage(Context context, OnDogChange callback) {
        this.context = context;
        this.callback = callback;
        this.service = RetrofitInstance
                .getRetrofitInstance()
                .create(GetDogDataService.class);
    }

    @Override
    public Dog getRandomDog() {

        mCall = service.getDogData();
        mCall.enqueue(new Callback<List<Dog>>() {
            @Override
            public void onResponse(Call<List<Dog>> call, Response<List<Dog>> response) {
                try {
                    if (response != null) {
                        Dog dog = response.body().get(0);
                        if (callback != null) {
                            callback.updateFragment(dog);
                            callback.updateStatusBar(dog);
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(context,
                            "onError:Something went wrong...Error message: " +
                                    e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Dog>> call, Throwable t) {
                Toast.makeText(context,
                        "onFailure:Something went wrong...Error message: " +
                                t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return new Dog("", "", "", "place_holder");
    }
}
