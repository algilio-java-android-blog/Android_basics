package modularization.libraries.ui_components.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AlertDialog;

import modularization.libraries.ui_components.R;

/**
 * A simple dialog containing an {@link SeekBar}.
 * <p>
 * See the <a href="{@docRoot}guide/topics/ui/controls/pickers.html">Pickers</a>
 * guide.
 */
public class SeekBarDialog extends AlertDialog implements OnClickListener,
        SeekBar.OnSeekBarChangeListener {

    private static final String MIN = "min";
    private static final String MAX = "max";
    private static final String PROGRESS = "progress";
    private static final String TV_PROGRESS_FORMAT = "Progress:%d/%d";


    final private TextView mTvProgress;
    private final SeekBar mSeekBar;

    private OnSeekBarSetListener mSeekBarSetListener;

    public interface OnSeekBarSetListener {
        void onProgressSet(SeekBar view, int progress);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private SeekBarDialog(SeekBarDialogBuilder builder) {
        super(builder.context, builder.themeResId);
        if (builder.title != null) {
            setTitle(builder.title);
        }

        final LayoutInflater inflater = LayoutInflater.from(builder.context);
        final View view = inflater.inflate(R.layout.dlg_seek_bar, null);
        setView(view);

        setButton(BUTTON_POSITIVE, builder.context.getString(R.string.ok), this);
        setButton(BUTTON_NEGATIVE, builder.context.getString(R.string.cancel), this);

        mTvProgress = view.findViewById(R.id.tvProgress);

        mSeekBar = view.findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(this);
        mSeekBar.setMin((int) builder.min);
        mSeekBar.setMax((int) builder.max);
        mSeekBar.setProgress(builder.progress);
        mSeekBarSetListener = builder.callback;
        setTvProgress(builder.progress, (int) builder.max);
    }

    @Override
    public void onClick(@NonNull DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                if (mSeekBarSetListener != null) {
                    mSeekBarSetListener.onProgressSet(mSeekBar, mSeekBar.getProgress());
                }
                break;
            case BUTTON_NEGATIVE:
                cancel();
                break;
        }
    }

    @NonNull
    public SeekBar getSeekBar() {
        return mSeekBar;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Bundle onSaveInstanceState() {
        final Bundle state = super.onSaveInstanceState();
        state.putInt(MIN, mSeekBar.getMin());
        state.putInt(MAX, mSeekBar.getMax());
        state.putInt(PROGRESS, mSeekBar.getProgress());
        return state;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int min = savedInstanceState.getInt(MIN);
        final int max = savedInstanceState.getInt(MAX);
        final int progress = savedInstanceState.getInt(PROGRESS);
        mSeekBar.setMin(min);
        mSeekBar.setMax(max);
        mSeekBar.setProgress(progress);
    }

    private void setTvProgress(int progress, int max) {
        mTvProgress.setText(String.format(TV_PROGRESS_FORMAT, progress, max));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        setTvProgress(seekBar.getProgress(), seekBar.getMax());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public static class SeekBarDialogBuilder {
        private final Context context;
        private String title;
        private int themeResId;
        private long min;
        private long max;
        private int progress;
        private OnSeekBarSetListener callback;


        public SeekBarDialogBuilder(Context context) {
            this.context = context;
        }

        public SeekBarDialogBuilder title(String title) {
            this.title = title;
            return this;
        }

        public SeekBarDialogBuilder callback(OnSeekBarSetListener callback) {
            this.callback = callback;
            return this;
        }

        public SeekBarDialogBuilder min(int min) {
            this.min = min;
            return this;
        }

        public SeekBarDialogBuilder max(int max) {
            this.max = max;
            return this;
        }

        public SeekBarDialogBuilder progress(long progress) {
            this.progress = (int) progress;
            return this;
        }

        public SeekBarDialogBuilder themesResID(int themeResId) {
            this.themeResId = themeResId;
            return this;
        }

        private void validate() throws IllegalStateException {
            if (context == null) {
                throw new IllegalStateException("context must not be null.");
            }

            if (themeResId == 0) {
                themeResId = resolveDialogTheme(context, themeResId);
            }

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public SeekBarDialog build() throws IllegalStateException {
            validate();
            return new SeekBarDialog(this);
        }

        static @StyleRes
        int resolveDialogTheme(@NonNull Context context, @StyleRes int themeResId) {
            if (themeResId == 0) {
                final TypedValue outValue = new TypedValue();
                context.getTheme().resolveAttribute(R.attr.alertDialogTheme, outValue, true);
                return outValue.resourceId;
            } else {
                return themeResId;
            }
        }
    }
}