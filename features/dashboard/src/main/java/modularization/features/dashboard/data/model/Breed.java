package modularization.features.dashboard.data.model;

import com.google.gson.annotations.SerializedName;

public class Breed {
    @SerializedName("weight")
    public Weight weight;
    @SerializedName("height")
    public Height height;
    @SerializedName("id")
    public int id;
    @SerializedName("bred_for")
    public String bred_for;
    @SerializedName("name")
    public String name;
    @SerializedName("breed_group")
    public String breed_group;
    @SerializedName("life_span")
    public String life_span;
    @SerializedName("temperament")
    public String temperament;
    @SerializedName("reference_image_id")
    public String reference_image_id;


    @Override
    public String toString() {
        return "Breed{" + "\n" +
                String.format("weight: imperial=%s, metric=%s \n ", weight.imperial, weight.metric) +
                String.format("height: imperial=%s, metric=%s \n ", height.imperial, height.metric) +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", breed_group='" + breed_group + '\'' + "\n" +
                ", life_span='" + life_span + '\'' +
                ", temperament='" + temperament + '\'' +
                ", reference_image_id='" + reference_image_id + '\'' +
                '}';
    }
}
