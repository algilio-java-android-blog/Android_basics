package modularization.libraries.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class TimerBroadcastService extends Service {

    private final static String TAG = TimerBroadcastService.class.getSimpleName();
    public static final String COUNTDOWN_BR = "com.example.basics.services.countdown_br";
    public static final String TIMER_SECONDS_UNTIL_FINISHED = "TIMER_SECONDS_UNTIL_FINISHED";
    public static final String TIMER_COUNT_DOWN_INTERVAL = "time_count_down_interval";
    public static final String TIMER_TICK_NO = "TIMER_TICK_NO";


    Intent mIntent = new Intent(COUNTDOWN_BR);
    CountDownTimer mCountDownTimer = null;
    private int mTick;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long mSecondsInFuture = intent.getLongExtra(
                TimerBroadcastService.TIMER_SECONDS_UNTIL_FINISHED,
                300);

        long mCountDownInterval = intent.getLongExtra(
                TimerBroadcastService.TIMER_COUNT_DOWN_INTERVAL,
                3);

        mTick = 0;

        Log.i(TAG, "Starting timer...");

        mCountDownTimer = new CountDownTimer(TimeUnit.SECONDS.toMillis(mSecondsInFuture),
                TimeUnit.SECONDS.toMillis(mCountDownInterval)) {

            @Override
            public void onTick(long millisUntilFinished) {
                mTick++;
                mIntent.putExtra(TIMER_SECONDS_UNTIL_FINISHED, millisUntilFinished);
                mIntent.putExtra(TIMER_TICK_NO, mTick);
                sendBroadcast(mIntent);
            }

            @Override
            public void onFinish() {
                mIntent.putExtra(TIMER_SECONDS_UNTIL_FINISHED, 0);
                sendBroadcast(mIntent);
                Log.i(TAG, "Timer finished.");
                stopSelf();
            }
        };

        mCountDownTimer.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        mCountDownTimer.cancel();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
