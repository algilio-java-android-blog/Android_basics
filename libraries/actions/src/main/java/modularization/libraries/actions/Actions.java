package modularization.libraries.actions;

import android.content.Context;
import android.content.Intent;

import org.jetbrains.annotations.NotNull;

public class Actions {

    static {
        INSTANCE = new Actions();
    }

    @NotNull
    public static final Actions INSTANCE;

    @NotNull
    public final Intent openDashboardIntent(@NotNull Context context) {
        Intent intent = this.internalIntent(context,
                "com.example.modularization.dashboard.open");
        return intent;
    }

    private final Intent internalIntent(Context context, String action) {
        return new Intent(action).setPackage(context.getPackageName());
    }

    private Actions() {
    }


}

