package modularization.features.dashboard.data.model;

import com.google.gson.annotations.SerializedName;

public class Height{
    @SerializedName("imperial")
    public String imperial;
    @SerializedName("metric")
    public String metric;
}